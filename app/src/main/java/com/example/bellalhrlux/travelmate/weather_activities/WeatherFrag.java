package com.example.bellalhrlux.travelmate.weather_activities;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFrag extends Fragment {

    private static final String BASE_URL_CURRENT = "http://api.openweathermap.org/data/2.5/";
    private static final String LOG_MSG = "weather_frag_error_1";
    private double latitude = 23.750854;
    private double longitude = 90.393527;
    private double searchLat = 23.750854;
    private double searchLong = 90.393527;
    private String tempUnit = "metric";
    private String unitType="C";
    ApiInterface apiInterface;
    private ProgressDialog progressDialog;


    //get location

    private FusedLocationProviderClient client;
    private double latitude_value, longitude_value;
    private LocationRequest request;
    private LocationCallback callback;
    private String cityName="";


    // private Response<CurrentWeather> response;

    private TextView locationName, maxTemp,minTemp, humidty, sunrise, sunset, presser,
            temperature, weatherName, weatherDescription,systemCurrentDate;
    private ImageView imageViewIcon;

    public WeatherFrag() {
        // Required empty public constructor

    }

    public void getApiResponse(String meg)
    {
        cityName=meg;
        // this.response=getResponse;
        //Toast.makeText(getContext(), "Get api method", Toast.LENGTH_SHORT).show();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_weather, container, false);
        //Toast.makeText(getContext(), "onCreate is called ", Toast.LENGTH_SHORT).show();
        //Log.e("MSG",cityName);
        //INITIALIZE OF LAYOUT ATTRIBUTES
        //Toast.makeText(getContext(), "isCelsius is "+SystemUtils.isCelsius, Toast.LENGTH_SHORT).show();
        if(InternetConnection.isNetworkConnected(getContext()))
        {
            locationName = v.findViewById(R.id.locationTV);
            maxTemp = v.findViewById(R.id.maxTemp);
            minTemp = v.findViewById(R.id.minTemp);
            humidty = v.findViewById(R.id.humidityTV);
            sunrise = v.findViewById(R.id.sunriseTV);
            sunset = v.findViewById(R.id.sunsetTV);
            presser = v.findViewById(R.id.presserTV);
            temperature = v.findViewById(R.id.tempTV);
            weatherName = v.findViewById(R.id.weatherName);
            imageViewIcon = v.findViewById(R.id.imageViewIcon);
            systemCurrentDate = v.findViewById(R.id.currentDate);

            progressDialog=new ProgressDialog(getContext());
            progressDialog.setTitle("Loading plz waiting.....");
            progressDialog.show();
            if(!cityName.isEmpty())
            {
                retrofitConnection();
            }


            weatherRetrofitAction();
            //check location is enable or not
            if(!SystemUtils.isLocationEnabled(getContext()) && cityName.isEmpty())
            {
                //do something
                progressDialog.dismiss();
                customAlertDialog("Warning!!","Your location service is not enable!!" +
                        "\n if you want to see weather for current location then " +
                        "you should be enable your location"+
                        " otherwise seen default location in dhaka city");

            }

            //displayOfData(response);

        }
        else {
            Toast.makeText(getContext(), "Internet connection is not available!!", Toast.LENGTH_SHORT).show();
        }
        return v;
    }


    public  void customAlertDialog(String title,String meg)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(meg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retrofitQuery(latitude,longitude);
            }
        });
        builder.show();
    }

    public void weatherRetrofitAction() {
        try{
            // Log.e("MSG", "Weather retrofit action begin " + cityName);
            //INITIALIZE OF RETROFIT



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_CURRENT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = retrofit.create(ApiInterface.class);

            //retrofitQuery(latitude,longitude);
            //INITIALIZE OF LOCATION ATTRIBUTIES

            client = LocationServices.getFusedLocationProviderClient(getActivity());
            request = new LocationRequest();
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            //request.setInterval(10000);
            //request.setFastestInterval(5000);


            callback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }

                    for (Location location : locationResult.getLocations()) {


                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        retrofitQuery(latitude,longitude);
                       // Toast.makeText(getContext(), "Changing location :"+latitude+" lan "+longitude, Toast.LENGTH_SHORT).show();

                        //latTV.setText(String.valueOf(latitude));
                        //lonTV.setText(String.valueOf(longitude));
                        // Toast.makeText(getContext(), "value: "+latitude, Toast.LENGTH_SHORT).show();


                    }
                }
            };
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        100);

            }
            client.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location == null) {
                        return;
                    }
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    //Toast.makeText(getContext(), "onSuccess lat lan :"+latitude+" lan "+longitude, Toast.LENGTH_SHORT).show();
                }
            });

            client.requestLocationUpdates(request, callback, null);



        }
        catch (Exception e)
        {
            Log.e(LOG_MSG,""+e);
        }





    }


    public void retrofitQuery(double lat,double lon)
    {


        try {
            //Log.e("latlong","after query :"+latitude+" "+longitude);
            String appId =getString(R.string.weather_api_id);
            String url = String.format("weather?lat=%f&lon=%f&units=%s&appid=%s",lat,lon,tempUnit,appId);
            //String url_2="weather?lat=23.750854&lon=90.393527&units=metric&appid=c86e44db3aa3b1c27ba6bc90b5ff75c3";

            Call<CurrentWeather> weatherCall = apiInterface.getCurrentWeather(url);

            if(InternetConnection.isNetworkConnected(getContext()))
            {
                weatherCall.enqueue(new Callback<CurrentWeather>() {
                    @Override
                    public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                        if(response.code() == 200)
                        {
                            // Log.e("MSG","weather fragment called");
                            //Toast.makeText(getContext(), "This is weather fragment", Toast.LENGTH_SHORT).show();
                            displayOfData(response);
                            progressDialog.dismiss();
                            //Toast.makeText(getContext(), "This is response method", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<CurrentWeather> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Response is failed :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                        //Toast.makeText(getContext(), "faild the connection or some error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else {
                Toast.makeText(getContext(), "Internet connection is not available!!", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e)
        {

        }
    }

    public void displayOfData(Response<CurrentWeather> response)
    {
        double temp,max,min;
        CurrentWeather currentWeather=response.body();
        String cityName=currentWeather.getName();
        if(SystemUtils.isCelsius)
        {
            temp=currentWeather.getMain().getTemp();
            max=currentWeather.getMain().getTempMax();
            min=currentWeather.getMain().getTempMin();
        }
        else {
            double getTemp=currentWeather.getMain().getTemp();
            double  getmax=currentWeather.getMain().getTempMax();
            double getMin=currentWeather.getMain().getTempMin();
            temp=SystemUtils.celsiusToFahrenhit(getTemp);
            max=SystemUtils.celsiusToFahrenhit(getmax);
            min=SystemUtils.celsiusToFahrenhit(getMin);
            unitType="F";
        }


        int humidityValue=currentWeather.getMain().getHumidity();
        double presserValue=currentWeather.getMain().getPressure();
        String countryName=currentWeather.getSys().getCountry();
        double getWinds=currentWeather.getWind().getSpeed();
        long sunriseValue=currentWeather.getSys().getSunrise()*1000L;
        long sunsetValue=currentWeather.getSys().getSunset()*1000L;
        // long todayDate=currentWeather.getDt()*1000;
        // Toast.makeText(getContext(), "Date :"+userDateFormat(todayDate), Toast.LENGTH_SHORT).show();
        //Log.d("value",""+cityName+" "+countryName+" "+temp+" "+maxTemp+" "+minTemp+" "+humidityValue);
        String currentDate=userDateFormat(sunriseValue);
        String sunsetTime=userDateFormat(sunsetValue);

        locationName.setText(cityName+", "+countryName);
        temperature.setText(temp+"\u00B0"+unitType);
        systemCurrentDate.setText(currentDateFormat());
        maxTemp.setText(max+"\u00B0"+unitType);
        minTemp.setText(min+"\u00B0"+unitType);
        humidty.setText(humidityValue+" %");
        presser.setText(presserValue+" hPa");
        weatherName.setText(currentWeather.getWeather().get(0).getMain()+"");

        sunrise.setText(""+currentDate);
        sunset.setText(""+sunsetTime);

        String photo = "https://openweathermap.org/img/w/"+currentWeather.getWeather().get(0).getIcon()+".png";
        Picasso.get().load(Uri.parse(photo)).into(imageViewIcon);

        //Log.e("key",formateDateOfTime(sunsetValue));
    }

    public void retrofitConnection()
    {
        //INITIALIZE OF RETROFIT
        // Log.e("MSG","Retrofit connection is begin");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_CURRENT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterface=retrofit.create(ApiInterface.class);



        // Log.e("MSG","User submitted city: "+cityName);
        String appId =getString(R.string.weather_api_id);
        String url = String.format("weather?q=%s&appid=%s",cityName,appId);
        //String url_2="weather?lat=23.750854&lon=90.393527&units=metric&appid=c86e44db3aa3b1c27ba6bc90b5ff75c3";

        Call<WeatherLocation> weatherCall = apiInterface.getWeatherLocation(url);

        if(InternetConnection.isNetworkConnected(getContext()))
        {
            weatherCall.enqueue(new Callback<WeatherLocation>() {
                @Override
                public void onResponse(Call<WeatherLocation> call, Response<WeatherLocation> response) {
                    if(response.code()==200)
                    {
                        // sendCity=cityName+" custom city";
                        //Toast.makeText(getContext(), "Sucessfully get lat long", Toast.LENGTH_SHORT).show();
                        WeatherLocation getLocation=response.body();
                        searchLat=getLocation.getCoord().getLat();
                        searchLong=getLocation.getCoord().getLon();
                        retrofitQuery(searchLat,searchLong);
                        //getActivity().finish();

                    }
                    else if(response.code()==404)
                    {
                        Toast.makeText(getContext(), "This city is not found!!", Toast.LENGTH_SHORT).show();
                        retrofitQuery(latitude,longitude);
                        //getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<WeatherLocation> call, Throwable t) {
                    Toast.makeText(getContext(), "Faild lat and lan: "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("response",""+t.getMessage());
                }
            });
        }
        else {
            Toast.makeText(getContext(), "Internet connection is not available!!", Toast.LENGTH_SHORT).show();
        }

    }

    public String formateDateOfTime(long timeInMillis)
    {
        Calendar now = Calendar.getInstance();
        Calendar lastCheckedCal = new GregorianCalendar();
        lastCheckedCal.setTimeInMillis(timeInMillis);
        Date lastCheckedDate = new Date(timeInMillis);
        String timeFormat = android.text.format.DateFormat.getTimeFormat(getContext()).format(lastCheckedDate);
        if (now.get(Calendar.YEAR) == lastCheckedCal.get(Calendar.YEAR) &&
                now.get(Calendar.DAY_OF_YEAR) == lastCheckedCal.get(Calendar.DAY_OF_YEAR)) {
            // Same day, only show time
            return timeFormat;
        } else {
            return android.text.format.DateFormat.getDateFormat(getContext()).format(lastCheckedDate) + " " + timeFormat;
        }
    }


    public String userDateFormat(long timeStamp)
    {
        Date d=new Date(timeStamp);
        //SimpleDateFormat dateFormat=new SimpleDateFormat("EEEE-MM-d hh:mm a");
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-d hh:mm a");
        String val=dateFormat.format(d);

        /*Date d1=new Date();
        //DateFormat format=DateFormat.getDateInstance();
        SimpleDateFormat format=new SimpleDateFormat("EEEE \n yyyy-MM-d");
        String val=format.format(d1);*/
        return val;
    }
    public String currentDateFormat()
    {
        Date d1=new Date();
        //DateFormat format=DateFormat.getDateInstance();
        SimpleDateFormat format=new SimpleDateFormat("\tEEEE \n yyyy-MM-d");
        String val=format.format(d1);
        return val;
    }

}
