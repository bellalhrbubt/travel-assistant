package com.example.bellalhrlux.travelmate.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.adapter_class.FireServiceAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fire_service_fm extends Fragment {

    String[] fireServiceNameList;
    String[] fireServiceContactsList;
    private ListView fireServiceContactLV;

    public Fire_service_fm() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fire_service_fm, container, false);
        //initialize of xml attributies
        fireServiceContactLV=view.findViewById(R.id.fire_service_LV);

        fireServiceNameList=getResources().getStringArray(R.array.distric_name);
        fireServiceContactsList=getResources().getStringArray(R.array.fire_service_contacts);
        FireServiceAdapter fireServiceAdapter=new FireServiceAdapter(getActivity(),fireServiceNameList,fireServiceContactsList);
        fireServiceContactLV.setAdapter(fireServiceAdapter);
        return view;
    }

}
