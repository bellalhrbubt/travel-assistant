package com.example.bellalhrlux.travelmate.pojo_class;

public class Uploads {
    private String imageId;
    private String mName;
    private String mImageUrl;
    private String createdTime;

    public Uploads()
    {

    }
    public Uploads(String imageId,String name,String imageUrl,String createdTime)
    {
        if(name.trim().equals(""))
        {
            name="No name";
        }
        mName=name;
        mImageUrl=imageUrl;
        this.imageId=imageId;
        this.createdTime=createdTime;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
