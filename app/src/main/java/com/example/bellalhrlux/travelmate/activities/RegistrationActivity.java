package com.example.bellalhrlux.travelmate.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.custom_utils.SystemUtils;
import com.example.bellalhrlux.travelmate.pojo_class.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrationActivity extends AppCompatActivity {
    private EditText nameET,emailET,phoneET,passET;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference rootRef, profileRef, userRef;
    private Task<AuthResult> resultTask;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registration);

        nameET=findViewById(R.id.uName);
        emailET=findViewById(R.id.uEmail);
        passET=findViewById(R.id.uPassword);
        phoneET=findViewById(R.id.uPhone);

        //INITIALIZE OF FIREBASE ATTRIBUITS
        auth=FirebaseAuth.getInstance();
        user=auth.getCurrentUser();
        rootRef = FirebaseDatabase.getInstance().getReference();
        profileRef=rootRef.child("Profile");

        //INITIALIZE OF PROGRESS DIALOG
        progressDialog=new ProgressDialog(this);
    }

    public void userRegistration(View view) {
        final String email=emailET.getText().toString().trim();
        String pass=passET.getText().toString().trim();
        final String name=nameET.getText().toString();
        final String phone=phoneET.getText().toString();
       if(!phone.isEmpty() && !name.isEmpty() && !email.isEmpty() && !pass.isEmpty())
       {
           progressDialog.setMessage("Loading plz waiting.....");
           progressDialog.show();

           resultTask=auth.createUserWithEmailAndPassword(email,pass);

           resultTask.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
               @Override
               public void onComplete(@NonNull Task<AuthResult> task) {
                   if(task.isSuccessful())
                   {
                       user=auth.getCurrentUser();
                       progressDialog.dismiss();
                       userRef=profileRef.child(user.getUid());
                       String key=user.getUid();
                       Users users=new Users(key,name,email,phone, SystemUtils.currentDate(),"","","");
                       userRef.setValue(users);
                       Toast.makeText(RegistrationActivity.this, "Successfully Create your acccount!", Toast.LENGTH_SHORT).show();
                       startActivity(new Intent(RegistrationActivity.this,MainActivity.class));
                   }
                   else {
                       progressDialog.dismiss();
                       Toast.makeText(RegistrationActivity.this, "Account has not been created!", Toast.LENGTH_SHORT).show();
                   }
               }
           });
       }
       else {
           Toast.makeText(this, "Please fill up every data fields!!", Toast.LENGTH_SHORT).show();
       }
    }


}
