package com.example.bellalhrlux.travelmate.Map;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.weather_activities.ApiInterface;
import com.example.bellalhrlux.travelmate.weather_activities.SystemUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback{
    private double latitude=23.8103, longitude=90.4125;
    private LocationRequest request;
    private LocationCallback callback;
    private FusedLocationProviderClient client;

    private GoogleMap map;
    private GoogleMapOptions mapOptions;
    private GeoDataClient geoDataClient;
    private PlaceDetectionClient placeDetectionClient;
    private ClusterManager<MyItems> clusterManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mapOptions=new GoogleMapOptions();
        mapOptions.zoomControlsEnabled(true);

        geoDataClient= Places.getGeoDataClient(this);
        placeDetectionClient=Places.getPlaceDetectionClient(this);

        SupportMapFragment mapFragment=SupportMapFragment.newInstance(mapOptions);
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction()
                .replace(R.id.mapContainer,mapFragment);
        ft.commit();
        mapFragment.getMapAsync(this);

        //check location is enable or not
        if(!SystemUtils.isLocationEnabled(this))
        {

            customAlertDialog("Wronging!!","Your location service is not enable!!" +
                    "\n if you want to see current location then " +
                    "you should be enable your location"+
                    " otherwise seen default location in dhaka city");

        }



    }
    public  void customAlertDialog(String title,String meg)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(meg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LatLng latLng=new LatLng(latitude,longitude);
                map.addMarker(new MarkerOptions().position(latLng));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
            }
        });
        builder.show();
    }

    //back button action method
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.currentPlace:
                //showCurrentPlaces();
                break;
            case R.id.pickPlace:
                pickAPlace();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
            map=googleMap;
            clusterManager = new ClusterManager<MyItems>(this,map);
            map.setOnMarkerClickListener(clusterManager);
            map.setOnCameraIdleListener(clusterManager);
            if (checkLocationPermission())
            {
                map.setMyLocationEnabled(true);
            }

            getLatLanFromDeviceLocation();
            //Toast.makeText(MapActivity.this, "onMap Ready is called", Toast.LENGTH_SHORT).show();

            /*LatLng latLng = new LatLng(23.7509,90.3935);
            //map.addMarker(new MarkerOptions().position(latLng).title("BITM").snippet("BDBL Karwanbazar"));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

            map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    map.addMarker(new MarkerOptions().position(latLng));
                }
            });*/

          //  showCurrentPlaces();
       // setMapMarker(23.7509,90.3935);



    }
    private void setMapMarker(double lat,double lon)
    {
        LatLng latLng = new LatLng(lat,lon);
        //map.addMarker(new MarkerOptions().position(latLng).title("BITM").snippet("BDBL Karwanbazar"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
        map.addMarker(new MarkerOptions().position(latLng));


        /*map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                Toast.makeText(MapActivity.this, "success setMapMarker", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void showCurrentPlaces() {
        if(checkLocationPermission())
            placeDetectionClient.getCurrentPlace(null)
                    .addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
                        @Override
                        public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                            if(task.isSuccessful() && task.getResult() != null){
                                PlaceLikelihoodBufferResponse response = task.getResult();
                                int count = response.getCount();
                                String[]placeNames = new String[count];
                                LatLng[] latLngs = new LatLng[count];
                                for(int i = 0; i < count; i++){
                                    PlaceLikelihood placeLikelihood = response.get(i);
                                    placeNames[i] = (String) placeLikelihood.getPlace().getName();
                                    latLngs[i] = placeLikelihood.getPlace().getLatLng();
                                    /*Toast.makeText(MapActivity.this, "placename "+
                                            placeNames[0]+" latlangs "+latLngs[0], Toast.LENGTH_SHORT).show();*/
                                }
                                putPlacesOnMap(placeNames,latLngs);
                                //Toast.makeText(MapActivity.this, "size "+count, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
    }

    private void putPlacesOnMap(String[] placeNames, LatLng[] latLngs) {
        List<MyItems> items = new ArrayList<>();
        for(int i = 0; i < placeNames.length; i++){
            MyItems item = new MyItems(latLngs[i],placeNames[i]);
            items.add(item);
        }
        clusterManager.addItems(items);
        clusterManager.cluster();
    }

    private boolean checkLocationPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},111);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void pickAPlace() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this),789);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 789 && resultCode == RESULT_OK){
            Place place = PlacePicker.getPlace(this,data);
            double latLngValue=place.getLatLng().latitude;
            double lon=place.getLatLng().longitude;
            setMapMarker(latLngValue,lon);
            Toast.makeText(this, place.getName(), Toast.LENGTH_SHORT).show();
        }
    }


    public void getLatLanFromDeviceLocation() {
        try{

            //INITIALIZE OF LOCATION ATTRIBUTIES

            client = LocationServices.getFusedLocationProviderClient(this);
            request = new LocationRequest();
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);



            callback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }

                    for (Location location : locationResult.getLocations()) {


                        latitude = location.getLatitude();
                        longitude = location.getLongitude();


                        LatLng latLng=new LatLng(latitude,longitude);
                        map.addMarker(new MarkerOptions().position(latLng));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
                       // Toast.makeText(MapActivity.this, "Changing location :"+latitude+" lan "+longitude, Toast.LENGTH_SHORT).show();


                    }
                }
            };
            if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        100);

            }
            client.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location == null) {
                        return;
                    }
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    //Toast.makeText(MapActivity.this, "onSuccess lat lan :"+latitude+" lan "+longitude, Toast.LENGTH_SHORT).show();
                }
            });

            client.requestLocationUpdates(request, callback, null);



        }
        catch (Exception e)
        {
            //Log.e(LOG_MSG,""+e);
        }

    }


}
