package com.example.bellalhrlux.travelmate.near_by_place;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.Map.MapActivity;
import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.weather_activities.ApiInterface;
import com.example.bellalhrlux.travelmate.weather_activities.CurrentWeather;
import com.example.bellalhrlux.travelmate.weather_activities.SystemUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NearByPlaceActivity extends AppCompatActivity {
    private Spinner placeSpinner,radiusSpinner;
    private Button searchBtn;
    private ListView listView;
    private static final String BASE_URL_NEARBY_PLACE= "https://maps.googleapis.com/maps/api/place/nearbysearch/";
    private static final String BASE_URL_PLACE_DETAILS= "https://maps.googleapis.com/maps/api/place/details/";
    ApiInterface apiInterface;
    double latitude=23.8050;
    double longitude=90.3635;
    private String selectedRadius=null;
    private String selectdPlace=null;
    private String latLon=null;
    private int placeIndex=0;
    private int radiusIndex=0;
    private int index,i;
    private List<PlaceDetailsPojo.Result> resultList = new ArrayList<>();
    private ProgressDialog progressDialog;

    private LocationRequest request;
    private LocationCallback callback;
    private FusedLocationProviderClient client;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by_place);

        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        placeSpinner=findViewById(R.id.placeType);
        radiusSpinner=findViewById(R.id.placeRadius);
        searchBtn=findViewById(R.id.searchBtn);
        listView=findViewById(R.id.nearPlaceLV);

        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Loading plz waiting...");

        final String placeType[]=getResources().getStringArray(R.array.place_type);
        String radius[]=getResources().getStringArray(R.array.place_radius);
        final String radiusValue[]=getResources().getStringArray(R.array.radius_value);

        ArrayAdapter<String> nearPlaceAdpater=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,placeType);
        placeSpinner.setAdapter(nearPlaceAdpater);
        ArrayAdapter<String> radiusAdpater=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,radius);
        radiusSpinner.setAdapter(radiusAdpater);

        radiusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position>0)
                {

                    selectedRadius=radiusValue[position-1];
                    //Toast.makeText(NearByPlaceActivity.this, ""+selectedRadius, Toast.LENGTH_SHORT).show();
                    radiusIndex=1;
                }
                else {
                    radiusIndex=0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        placeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position>0)
                {
                    selectdPlace=placeType[position];
                    //Toast.makeText(NearByPlaceActivity.this, ""+selectdPlace, Toast.LENGTH_SHORT).show();
                    placeIndex=1;
                }
                else {
                    placeIndex=0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(placeIndex==1 && radiusIndex==1)
                    {
                        resultList.clear();
                        getLatLanFromDeviceLocation();
                        progressDialog.show();
                    }
                    else {
                        //Toast.makeText(NearByPlaceActivity.this, "No", Toast.LENGTH_SHORT).show();
                        SystemUtils.customAlertDialog(NearByPlaceActivity.this,"Wrong","Please select place and radius!!");
                    }
            }
        });


        if(!SystemUtils.isLocationEnabled(this))
        {
            SystemUtils.customAlertDialog(this,"Wronging",
                    "Your location service is not enable!!" +
                            "\n if you want to search nearest  location then " +
                            "you should be enable your location");
        }
       /* else{
            String latLng=latitude+","+longitude;
            retrofitConnection(latLng);
        }*/

       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Toast.makeText(NearByPlaceActivity.this, "Position is "
                       +resultList.get(position).getGeometry().getLocation().getLat(), Toast.LENGTH_SHORT).show();
               Intent intent =new Intent(NearByPlaceActivity.this,NearPlaceDetailsActivity.class);
               double lat=resultList.get(position).getGeometry().getLocation().getLat();
               double lan=resultList.get(position).getGeometry().getLocation().getLng();
               intent.putExtra("lat",lat);
               intent.putExtra("lan",lan);
               intent.putExtra("currentLat",latitude);
               intent.putExtra("currentLan",longitude);
               startActivity(intent);

           }
       });

    }



    //back button action method
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void retrofitConnection(String latLng)
    {
        try{
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_NEARBY_PLACE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = retrofit.create(ApiInterface.class);

            String key =getString(R.string.nearBy_place_api_key);
            //String latLng=latitude+","+longitude;
            final String url = String.format("json?location=%s&radius=%s&type=%s&key=%s",latLng,selectedRadius,selectdPlace,key);
            //Log.e("url",BASE_URL_NEARBY_PLACE+url);


            Call<NearByPlacePojo> nearPlace = apiInterface.getNearPlace(url);
            nearPlace.enqueue(new Callback<NearByPlacePojo>() {
                @Override
                public void onResponse(Call<NearByPlacePojo> call, Response<NearByPlacePojo> response) {
                    if(response.code()==200)
                    {

                        NearByPlacePojo nearByPlacePojo=response.body();
                        //int value=nearByPlacePojo.getResults().size();
                        List<NearByPlacePojo.Result> getResult=nearByPlacePojo.getResults();
                        Log.e("URL",""+BASE_URL_NEARBY_PLACE+url);
                    /*Toast.makeText(NearByPlaceActivity.this, "ok fine size "+getResult.size(), Toast.LENGTH_SHORT).show();

                       SystemUtils.customAlertDialog(NearByPlaceActivity.this,"Found",
                               "Total number of "+selectdPlace+" is "+getResult.size());
                       NearByPlaceAdapter adapter=new NearByPlaceAdapter(NearByPlaceActivity.this,getResult);
                       listView.setAdapter(adapter);*/


                       /*SystemUtils.customAlertDialog(NearByPlaceActivity.this,"Not found",
                               "This item has not been found!!");
                       NearByPlaceAdapter adapter=new NearByPlaceAdapter(NearByPlaceActivity.this,getResult);
                       listView.setAdapter(adapter);*/


                        //Toast.makeText(NearByPlaceActivity.this, "List size "+getResult.size(), Toast.LENGTH_SHORT).show();

                        if(getResult.size()>0)
                        {
                            progressDialog.dismiss();
                            SystemUtils.customAlertDialog(NearByPlaceActivity.this,"Success Results",
                                    getResult.size()+ "  number of "+selectdPlace+" has been found!!");
                            getPlaceDetails(getResult);
                        }
                        else {
                            progressDialog.dismiss();
                            SystemUtils.customAlertDialog(NearByPlaceActivity.this,"Failed Results",
                                    selectdPlace+" is not found on this redius "+selectedRadius+"m");
                        }


                  /*  double l=nearByPlacePojo.getResults().get(1).getGeometry().getLocation().getLat();
                    double lan= nearByPlacePojo.getResults().get(1).getGeometry().getLocation().getLng();
                    Log.e("name",l+","+lan);*/
                        // Toast.makeText(NearByPlaceActivity.this, "Successfully get near place Size is "+value, Toast.LENGTH_SHORT).show();
                    }
                    else if(response.code()==400)
                    {
                        Toast.makeText(NearByPlaceActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                    }

                    else {
                        Toast.makeText(NearByPlaceActivity.this, "Some error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NearByPlacePojo> call, Throwable t) {
                    Toast.makeText(NearByPlaceActivity.this, "error is ", Toast.LENGTH_SHORT).show();
                    Log.e("near","Error "+t.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private void getPlaceDetails(final List<NearByPlacePojo.Result> results)
    {

       try{
           if(results.size()>0)
           {
               for(i=0;i<results.size();i++)
               {
                   index=i;
                   //Toast.makeText(this, "Value "+results.get(i).getName(), Toast.LENGTH_SHORT).show();
                   Retrofit retrofit = new Retrofit.Builder()
                           .baseUrl(BASE_URL_PLACE_DETAILS)
                           .addConverterFactory(GsonConverterFactory.create())
                           .build();
                   apiInterface = retrofit.create(ApiInterface.class);

                   String key =getString(R.string.nearBy_place_api_key);
                   String placeId=results.get(i).getPlaceId();
                   String url = String.format("json?placeid=%s&key=%s",placeId,key);
                   Call<PlaceDetailsPojo> placeDetails = apiInterface.getPlaceDetails(url);
                   placeDetails.enqueue(new Callback<PlaceDetailsPojo>() {
                       @Override
                       public void onResponse(Call<PlaceDetailsPojo> call, Response<PlaceDetailsPojo> response) {
                           if(response.code()==200)
                           {


                               PlaceDetailsPojo detailsPojo= response.body();
                               PlaceDetailsPojo.Result result=detailsPojo.getResult();


                               resultList.add(result);
                               //Toast.makeText(NearByPlaceActivity.this, "Successfull 200"+result.getAdrAddress(), Toast.LENGTH_SHORT).show();

                               //when loop will be finished

                               if(resultList.size()>0)
                               {
                                   if(index==(results.size())-1)
                                   {
                                       //Toast.makeText(NearByPlaceActivity.this, "Now it is execute", Toast.LENGTH_SHORT).show();
                                       PlaceAdapter adapter=new PlaceAdapter(NearByPlaceActivity.this,resultList);
                                       listView.setAdapter(adapter);
                                       //Toast.makeText(NearByPlaceActivity.this, "Now it is execute", Toast.LENGTH_SHORT).show();
                                   }
                                   else {
                                       Toast.makeText(NearByPlaceActivity.this, "Any items has not been founds!!", Toast.LENGTH_LONG).show();
                                   }
                                   //Toast.makeText(NearByPlaceActivity.this, "Value is "+resultList.get(0).getAdrAddress(), Toast.LENGTH_SHORT).show();
                               }


                           }
                           if(response.code()==400)
                           {
                               Toast.makeText(NearByPlaceActivity.this, "Not found 400", Toast.LENGTH_SHORT).show();
                           }
                       }


                       @Override
                       public void onFailure(Call<PlaceDetailsPojo> call, Throwable t) {
                           Toast.makeText(NearByPlaceActivity.this, "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });
                   //Toast.makeText(NearByPlaceActivity.this, "i  "+i, Toast.LENGTH_SHORT).show();

               }




               // PlaceAdapter adapter=new PlaceAdapter(this,resultList);
               //listView.setAdapter(adapter);

           }
       }
       catch (Exception e)
       {
           Toast.makeText(this, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
       }

    }


    public void getLatLanFromDeviceLocation() {
        try{

            //INITIALIZE OF LOCATION ATTRIBUTIES

            client = LocationServices.getFusedLocationProviderClient(this);
            request = new LocationRequest();
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);



            callback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }

                    for (Location location : locationResult.getLocations()) {


                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        latLon=latitude+","+longitude;
                        retrofitConnection(latLon);
                        //LatLng latLng=new LatLng(latitude,longitude);


                    }
                }
            };
            if (ActivityCompat.checkSelfPermission(NearByPlaceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(NearByPlaceActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        100);

            }
            client.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location == null) {
                        return;
                    }
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    //Toast.makeText(MapActivity.this, "onSuccess lat lan :"+latitude+" lan "+longitude, Toast.LENGTH_SHORT).show();
                }
            });

            client.requestLocationUpdates(request, callback, null);



        }
        catch (Exception e)
        {
            //Log.e(LOG_MSG,""+e);
        }

    }



}
