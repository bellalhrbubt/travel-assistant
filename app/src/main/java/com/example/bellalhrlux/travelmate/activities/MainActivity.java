package com.example.bellalhrlux.travelmate.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.custom_utils.SystemUtils;
import com.example.bellalhrlux.travelmate.weather_activities.InternetConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private EditText uEmailET,uPassET;
    private FirebaseAuth auth;
    private FirebaseUser users;
    private Task<AuthResult> taskResult;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        int index=1;
        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        //write user email and password
         sharedPref = getPreferences(Context.MODE_PRIVATE);
         editor = sharedPref.edit();
         editor.putString("key","bellal");
         editor.putString("pass","123456");

        //Read user email and password
        String highScore = sharedPref.getString("key","Not found");
        String password = sharedPref.getString("logged","Pass Not found");
        //Toast.makeText(this, "Value is "+highScore.toString()+" "+password, Toast.LENGTH_SHORT).show();
        if(index==0)
        {
            startActivity(new Intent(MainActivity.this,HomeEventActivity.class));
            finish();
        }
        else {
            setContentView(R.layout.activity_main);
        }



        uEmailET=findViewById(R.id.uEmail);
        uPassET=findViewById(R.id.uPassword);

        progressDialog=new ProgressDialog(this);

        //INITIALIZE OF FIREBASE ATTRIBUTIS
        auth=FirebaseAuth.getInstance();
        users=auth.getCurrentUser();

        //SystemUtils.dateDifference();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");


        try {
            Date startDate = simpleDateFormat.parse("10/10/2012 15:30:10");
            Date endDate = simpleDateFormat.parse("13/11/2013 20:35:55");
            SystemUtils.printDifference(startDate,endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        editor.commit();

        if(InternetConnection.isNetworkConnected(this))
        {

        }
        else {
           SystemUtils.customAlertMessage(this,"Your internet connection is not available!!");
        }

    }

    public void registrationMethod(View view) {
        startActivity(new Intent(this,RegistrationActivity.class));
    }

    public void userLogin(View view) {
        if(InternetConnection.isNetworkConnected(this))
        {
            try{
                String email=uEmailET.getText().toString();
                String pass=uPassET.getText().toString();
                if(!email.isEmpty() && !pass.isEmpty())
                {
                    progressDialog.setMessage("Loading plz wait.....");
                    progressDialog.show();

                    taskResult=auth.signInWithEmailAndPassword(email,pass);
                    taskResult.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            if(task.isSuccessful())
                            {
                                editor.putString("logged","1");
                                startActivity(new Intent(MainActivity.this,HomeEventActivity.class));
                                finish();
                            }
                            else {
                                Toast.makeText(MainActivity.this, ""+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    taskResult.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            //Toast.makeText(MainActivity.this, "Login is failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else{
                    Toast.makeText(this, "Fill up all data fields!!", Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e)
            {
                //Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        else {
            SystemUtils.customAlertMessage(this,"Your internet connection is not available!!");
        }

    }
}
