package com.example.bellalhrlux.travelmate.near_by_place;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bellalhrlux.travelmate.R;

import java.util.List;

public class NearByPlaceAdapter extends ArrayAdapter<NearByPlacePojo.Result> {
    private List<NearByPlacePojo.Result> placeList;
    Context context;
    public NearByPlaceAdapter(@NonNull Context context, List<NearByPlacePojo.Result> listItem) {
        super(context, R.layout.nearest_place_row,listItem);
        this.context=context;
        placeList =listItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=layoutInflater.inflate(R.layout.nearest_place_row,parent,false);
        TextView placeName=convertView.findViewById(R.id.placeName);
        //TextView exDescription=convertView.findViewById(R.id.description);
       // TextView createTime=convertView.findViewById(R.id.createdDate);


        placeName.setText(placeList.get(position).getName()+"");
        //exDescription.setText("Description : "+ placeList.get(position).getDescription()+"");
        //createTime.setText("Create Time : "+ placeList.get(position).getCreatedTime()+"");

        return convertView;
    }
}
