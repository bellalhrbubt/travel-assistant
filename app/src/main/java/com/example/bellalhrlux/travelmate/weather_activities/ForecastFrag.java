package com.example.bellalhrlux.travelmate.weather_activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastFrag extends Fragment {

    private static final String BASE_URL_CURRENT = "http://api.openweathermap.org/data/2.5/";
    private double latitude = 23.750854;
    private double searchLat = 23.750854;
    private double longitude = 90.393527;
    private double searchLong = 90.393527;
    private String tempUnit = "metric";
    ApiInterface apiInterface;

    List<Forecast.List> getList;
    ForecastAdapter forecastAdapter;
    RecyclerView recyclerView;
    private String cityName="";


    public ForecastFrag() {
        // Required empty public constructor
    }

    public void getUserSearchCity(String city)
    {
        cityName=city;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_forecast, container, false);

        recyclerView=v.findViewById(R.id.recyclerView);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_CURRENT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterface=retrofit.create(ApiInterface.class);
        // Toast.makeText(getContext(), "City name "+cityName, Toast.LENGTH_SHORT).show();
        if(!cityName.isEmpty())
        {
            retrofitConnection();
        }
        else {
            getForcastValue(latitude,longitude);
        }


        return v;

    }

    public void getForcastValue(double lat,double lan)
    {
        String appId ="c86e44db3aa3b1c27ba6bc90b5ff75c3";
        String url = String.format("forecast?lat=%f&lon=%f&units=%s&cnt=7&appid=%s",lat,lan,tempUnit,appId);
        //String url_2="weather?lat=23.750854&lon=90.393527&units=metric&appid=c86e44db3aa3b1c27ba6bc90b5ff75c3";

        Call<Forecast> forecastCall = apiInterface.getForecast(url);

        forecastCall.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
                if (response.code()==200)
                {
                    try{
                        //Toast.makeText(getContext(), "Forecast is successfully execute", Toast.LENGTH_SHORT).show();
                        Forecast forecast=response.body();
                        //Toast.makeText(getActivity(), ""+forecast.getCity().getCountry(), Toast.LENGTH_SHORT).show();
                        getList = forecast.getList();
                        //Toast.makeText(getActivity(), "size: "+getList.size(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getContext(), "City name :"+forecast.getCity(), Toast.LENGTH_SHORT).show();
                        //int value=forecast.getList().size();
                        //double d=forecast.getList().get(0).getMain().getPressure();

                        //Toast.makeText(getContext(), pressure+"", Toast.LENGTH_SHORT).show();



                        try{
                            forecastAdapter=new ForecastAdapter(getList);
                            LinearLayoutManager llm=new LinearLayoutManager(getActivity());
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            recyclerView.setLayoutManager(llm);
                            recyclerView.setAdapter(forecastAdapter);
                        }
                        catch(Exception e)
                        {
                            Toast.makeText(getContext(), "forecast adapter list view error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e)
                    {
                        //Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                        Log.e("forcast",""+e);
                    }


                }
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {
                Toast.makeText(getContext(), "Forecast is not execute", Toast.LENGTH_SHORT).show();
                Log.e("retrofit", "onFailure: "+t.getMessage());
            }
        });
    }


    public void retrofitConnection()
    {
        //INITIALIZE OF RETROFIT
        // Log.e("MSG","Retrofit connection is begin");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_CURRENT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterface=retrofit.create(ApiInterface.class);



        // Log.e("MSG","User submitted city: "+cityName);
        String appId =getString(R.string.weather_api_id);
        String url = String.format("weather?q=%s&appid=%s",cityName,appId);
        //String url_2="weather?lat=23.750854&lon=90.393527&units=metric&appid=c86e44db3aa3b1c27ba6bc90b5ff75c3";

        Call<WeatherLocation> weatherCall = apiInterface.getWeatherLocation(url);

        if(InternetConnection.isNetworkConnected(getContext()))
        {
            weatherCall.enqueue(new Callback<WeatherLocation>() {
                @Override
                public void onResponse(Call<WeatherLocation> call, Response<WeatherLocation> response) {
                    if(response.code()==200)
                    {
                        // sendCity=cityName+" custom city";
                        //Toast.makeText(getContext(), "Sucessfully get lat long", Toast.LENGTH_SHORT).show();
                        WeatherLocation getLocation=response.body();
                        searchLat=getLocation.getCoord().getLat();
                        searchLong=getLocation.getCoord().getLon();
                        getForcastValue(searchLat,searchLong);

                    }
                    else if(response.code()==404)
                    {
                        Toast.makeText(getContext(), "This city is not found!!", Toast.LENGTH_SHORT).show();
                        getForcastValue(latitude,longitude);
                    }
                }

                @Override
                public void onFailure(Call<WeatherLocation> call, Throwable t) {
                    Toast.makeText(getContext(), "Faild lat and lan: "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("response",""+t.getMessage());
                }
            });
        }
        else {
            Toast.makeText(getContext(), "Internet connection is not available!!", Toast.LENGTH_SHORT).show();
        }

    }

}
