package com.example.bellalhrlux.travelmate.adapter_class;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.custom_utils.SystemUtils;
import com.example.bellalhrlux.travelmate.pojo_class.Events;
import com.example.bellalhrlux.travelmate.pojo_class.Expenses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ExpenseAdapter extends ArrayAdapter<Expenses> {
    private List<Expenses> expensesList;
    Context context;
    public ExpenseAdapter(@NonNull Context context, List<Expenses> listItem) {
        super(context, R.layout.expense_row,listItem);
        this.context=context;
        expensesList =listItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=layoutInflater.inflate(R.layout.expense_row,parent,false);
        TextView exAmount=convertView.findViewById(R.id.amount);
        TextView exDescription=convertView.findViewById(R.id.description);
        TextView createTime=convertView.findViewById(R.id.createdDate);


        exAmount.setText("Amount  : "+expensesList.get(position).getAmount()+"tk");
        exDescription.setText("Description : "+expensesList.get(position).getDescription()+"");
        createTime.setText("Create Time : "+expensesList.get(position).getCreatedTime()+"");

        return convertView;
    }
}
