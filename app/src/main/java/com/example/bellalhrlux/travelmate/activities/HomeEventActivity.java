package com.example.bellalhrlux.travelmate.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.BloodDonorList;
import com.example.bellalhrlux.travelmate.Map.MapActivity;
import com.example.bellalhrlux.travelmate.EmergencyContacts;
import com.example.bellalhrlux.travelmate.User_Account;
import com.example.bellalhrlux.travelmate.near_by_place.NearByPlaceActivity;
import com.example.bellalhrlux.travelmate.pojo_class.Events;
import com.example.bellalhrlux.travelmate.adapter_class.EventsAdapter;
import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.custom_utils.SystemUtils;
import com.example.bellalhrlux.travelmate.pojo_class.Users;
import com.example.bellalhrlux.travelmate.weather_activities.Weather_Activity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HomeEventActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener{
    private EditText eNameET,eStartTE,eDestinationET,eBudgetET,eStartDate;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    String getSelectedDate ="";
    private DatabaseReference rootRef,eventRef,userRef, eventListRef,sendDbRef,logedUserRef;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private ListView listView;
    TextView getTV,emergencyContactTV;
    private List<Events> getEventList=new ArrayList<>();
    //navigation drawerLayout code
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private TextView weatherTV,pickNearPlaceTV,blood_donorTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home_event);


        //initialize of navigation bar
        mDrawerLayout=findViewById(R.id.drawer);

        mToggle=new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setNavigationViewListner();





        auth=FirebaseAuth.getInstance();
        user=auth.getCurrentUser();
        rootRef = FirebaseDatabase.getInstance().getReference();
        eventRef=rootRef.child("Events");
        userRef=eventRef.child(user.getUid());
        eventListRef =userRef.child("Event List");

        //Initialize of textview
        weatherTV=findViewById(R.id.weatherTV);
        pickNearPlaceTV=findViewById(R.id.pickNearPlacesTV);
        emergencyContactTV=findViewById(R.id.emergencyContacts);
        blood_donorTV=findViewById(R.id.blood_donor);

        //add listener data
        weatherTV.setOnClickListener(this);
        pickNearPlaceTV.setOnClickListener(this);
        emergencyContactTV.setOnClickListener(this);
        blood_donorTV.setOnClickListener(this);

        //INITIALIZE OF LISTVIEW
        //listView=findViewById(R.id.listView);
        //getTV=findViewById(R.id.messageTV);
        //getUserCreatedEvent();

        //listViewCrud();
        //showDetails();

        //dynamically set value for drawer header
        setDrawerHeader();

    }

    private void setDrawerHeader()
    {
        final List<Users> getUserList=new ArrayList<>();
        logedUserRef=rootRef.child("Profile").child(user.getUid());
        logedUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //getUserList.clear();
                String name=null;
                String email=null;
                String bloodGroup=null;
                String age=null;
                String distric=null;
              /* for(DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                   //Users users=snapshot.getValue(Users.class);
                    //getUserList.add(users);

                    name=snapshot.child("userName").getValue().toString();

                }*/
                name= (String) dataSnapshot.child("userName").getValue();
                email= (String) dataSnapshot.child("userEmail").getValue();
                bloodGroup= (String) dataSnapshot.child("blood_group").getValue();
                age= (String) dataSnapshot.child("age").getValue();
                distric= (String) dataSnapshot.child("distric").getValue();

                if(bloodGroup.equals("") && age.equals("") && distric.equals(""))
                {
                    updateAlertDialog();
                }





                NavigationView navigationView = (NavigationView) findViewById(R.id.navigationViewId);
                View headerView = navigationView.getHeaderView(0);
                TextView logedUsername=headerView.findViewById(R.id.userName);
                TextView logedEmail=headerView.findViewById(R.id.userEmail);
                logedUsername.setText(name);
                logedEmail.setText(email);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //for navigation code
        if(mToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        //for navigation code end

        switch (item.getItemId())
        {
            case R.id.logout:
                //Toast.makeText(this, "Logout menu is click", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(HomeEventActivity.this,MainActivity.class));
                finish();
                break;
            case R.id.profile:
                startActivity(new Intent(HomeEventActivity.this,User_Account.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }





    //LIST VIEW ITEM CRUD OPERATION CODE END

    //GET DETAILS FOR SPECIFIC LIST ITEM
    public void showDetails()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(HomeEventActivity.this, "name "+getEventList.get(position).getEventName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String calender()
    {
        DatePicker datePicker=new DatePicker(this);
        int currentYear=datePicker.getYear();
        int currentMonth=datePicker.getMonth();
        int currentDate=datePicker.getDayOfMonth();

         datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
             @Override
             public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                 //Toast.makeText(HomeEventActivity.this, year+"/"+month+"/"+dayOfMonth, Toast.LENGTH_SHORT).show();
                 getSelectedDate=dayOfMonth+"/"+(month+1)+"/"+year;




                 try {
                     // CHECK THE SELECTED DATE IS VALID OR INVALID...
                     SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");
                     Date startDate = simpleDateFormat.parse(SystemUtils.currentDate());
                     Date endDate = simpleDateFormat.parse(getSelectedDate);
                     long val=Long.valueOf(SystemUtils.printDifference(startDate,endDate));
                     if(val>=0)
                     {
                         eStartDate.setText(getSelectedDate);
                     }
                     else {
                            eStartDate.setText("");
                            SystemUtils.messageAlert(HomeEventActivity.this,"Error","You should be select " +
                                    "current or future date!");
                     }

                 } catch (ParseException e) {
                     e.printStackTrace();
                 }

                 // END THIS CODE
             }
         },currentYear,currentMonth,currentDate);
         datePickerDialog.show();

         return getSelectedDate;
    }
    public void timePicker()
    {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

            }
        }, hour, minute,false);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.nearBy:
                Intent nearIntent=new Intent(HomeEventActivity.this, NearByPlaceActivity.class);
                startActivity(nearIntent);
                break;
            case R.id.map:
                Intent mapIntent=new Intent(HomeEventActivity.this, MapActivity.class);
                startActivity(mapIntent);
                //Toast.makeText(this, "Map is selected", Toast.LENGTH_SHORT).show();
                break;

        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationViewId);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.weatherTV:
                //Toast.makeText(this, "weather is selected", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(HomeEventActivity.this, Weather_Activity.class);
                startActivity(intent);
                break;
            case R.id.pickNearPlacesTV:
                startActivity(new Intent(HomeEventActivity.this,MapActivity.class));
                break;
            case R.id.emergencyContacts:
                startActivity(new Intent(HomeEventActivity.this,EmergencyContacts.class));
                break;
            case R.id.blood_donor:
                startActivity(new Intent(HomeEventActivity.this,BloodDonorList.class));
                break;
        }
    }

    private void updateAlertDialog()
    {

            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("Warning!!");
            builder.setMessage("Please update your profile!!");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent=new Intent(HomeEventActivity.this,User_Account.class);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();


    }
}
