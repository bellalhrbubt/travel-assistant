package com.example.bellalhrlux.travelmate.pojo_class;

import java.io.Serializable;

public class Users implements Serializable {
    private String userId;
    private String userName;
    private String userEmail;
    private String userPhone;
    private String createDate;
    private String blood_group;
    private String age;
    private String distric;

    public Users(String userId, String userName,String userEmail, String userPhone,String createDate,String blood_group,String age,String distric) {
        this.userId = userId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.createDate=createDate;
        this.userEmail=userEmail;
        this.age=age;
        this.distric=distric;
        this.blood_group=blood_group;
    }

    public Users(String userName,String userEmail, String userPhone,String createDate,String blood_group,String age,String distric) {
        this.userName = userName;
        this.userPhone = userPhone;
        this.createDate=createDate;
        this.userEmail=userEmail;
        this.age=age;
        this.distric=distric;
        this.blood_group=blood_group;
    }
    public Users()
    {
        //default constractor
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDistric() {
        return distric;
    }

    public void setDistric(String distric) {
        this.distric = distric;
    }
}
