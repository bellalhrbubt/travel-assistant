package com.example.bellalhrlux.travelmate;

import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.pojo_class.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class User_Account extends AppCompatActivity implements View.OnClickListener{
    private DatabaseReference rootRef,eventRef,userRef, profileUpdateRef,sendDbRef,logedUserRef;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private ArrayList<Users> getUsersList=new ArrayList<>();
    private ImageView backBtn;
    private TextView userNameTV,userEmailTV,userContactTV,bloodGroupTV,ageTV,districTV,updateUserInforBtn,updatePassword;
    private String[] bloodGroupList;
    private String[] districtNameList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__account);

        //Initialize of firebase
        auth=FirebaseAuth.getInstance();
        user=auth.getCurrentUser();
        rootRef=FirebaseDatabase.getInstance().getReference();
        userRef=rootRef.child("Profile");

        //Initialize of layout attribuits
        backBtn=findViewById(R.id.backBtn);
        userNameTV=findViewById(R.id.loggedUserName);
        userEmailTV=findViewById(R.id.loggedUserEmail);
        userContactTV=findViewById(R.id.loggedUserContact);
        bloodGroupTV=findViewById(R.id.blood_group);
        ageTV=findViewById(R.id.age);
        districTV=findViewById(R.id.distric);
        updatePassword=findViewById(R.id.updatePassword);
        updateUserInforBtn=findViewById(R.id.updateUserInforBtn);

        //add button listener
        updateUserInforBtn.setOnClickListener(this);
        updatePassword.setOnClickListener(this);

        //getLoginUserInformation
        getLoggedUserInfor();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void getLoggedUserInfor() {
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getUsersList.clear();
                for(DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                    Users users=snapshot.getValue(Users.class);//this not efficient way
                    if(users.getUserId().equals(user.getUid()))
                    {
                        getUsersList.add(users);
                    }
                }

                userNameTV.setText(getUsersList.get(0).getUserName());
                userEmailTV.setText(getUsersList.get(0).getUserEmail());
                userContactTV.setText(getUsersList.get(0).getUserPhone());

                if(getUsersList.get(0).getBlood_group().equals(""))
                {
                    bloodGroupTV.setText("N/A");
                }
                else{
                    bloodGroupTV.setTextColor(Color.parseColor("#159F5C"));
                    bloodGroupTV.setText(getUsersList.get(0).getBlood_group());
                }
                if(getUsersList.get(0).getDistric().equals(""))
                {
                    districTV.setText("N/A");
                }
                else{
                    districTV.setText(getUsersList.get(0).getDistric());
                }
                if(getUsersList.get(0).getAge().equals(""))
                {
                    ageTV.setText("N/A");
                }
                else{
                    ageTV.setText(getUsersList.get(0).getAge());

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.updateUserInforBtn:
                updateUserInfor();
                break;
        }
    }

    private void updateUserInfor() {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View deleteDialogView = inflater.inflate(R.layout.alert_user_infor_update, null);
        final AlertDialog dialog = new AlertDialog.Builder(this).create();

        Button cancelBtn=deleteDialogView.findViewById(R.id.cancelBtn);
        Button sendBtn=deleteDialogView.findViewById(R.id.sendBtn);
        final Spinner bloodGroupSpinner=deleteDialogView.findViewById(R.id.blood_group_spinner);
        final Spinner districSpinner=deleteDialogView.findViewById(R.id.distric_spinner);
        final TextView age=deleteDialogView.findViewById(R.id.age);
        dialog.setView(deleteDialogView);





        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



        bloodGroupList=getResources().getStringArray(R.array.blood_group);
        districtNameList=getResources().getStringArray(R.array.distric_name_1);
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this,R.layout.spinner_row,R.id.spinnerValueTV,bloodGroupList);
        bloodGroupSpinner.setAdapter(adapter);
        ArrayAdapter<String> adapter1=new ArrayAdapter<>(this,R.layout.spinner_row,R.id.spinnerValueTV,districtNameList);
        districSpinner.setAdapter(adapter1);
        //for selected blood group
        if(!getUsersList.get(0).getBlood_group().equals(""))
        {
            bloodGroupSpinner.setSelection(adapter.getPosition(getUsersList.get(0).getBlood_group()));
        }
        //for selected district
        if(!getUsersList.get(0).getDistric().equals(""))
        {
            districSpinner.setSelection(adapter1.getPosition(getUsersList.get(0).getDistric()));
        }
        if(!getUsersList.get(0).getAge().equals(""))
        {
            age.setText(getUsersList.get(0).getAge());
        }

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(User_Account.this, "Send Button", Toast.LENGTH_SHORT).show();
                String bloodGroupName=bloodGroupSpinner.getSelectedItem().toString();
                String district=districSpinner.getSelectedItem().toString();
                String ageValue=age.getText().toString();
                if(!bloodGroupName.isEmpty() && !district.isEmpty() && !ageValue.isEmpty())
                {
                    setUsersUpdateInfor(dialog,bloodGroupName,district,ageValue);
                }
                else{
                    Toast.makeText(User_Account.this, "Fill up all data fields!!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        dialog.show();
    }

    private void setUsersUpdateInfor(AlertDialog dialog,String bloodGroupName,String district,String age)
    {
        profileUpdateRef=userRef.child(user.getUid());
        profileUpdateRef.child("blood_group").setValue(bloodGroupName);
        profileUpdateRef.child("distric").setValue(district);
        profileUpdateRef.child("age").setValue(age);
        //update the value after successfully insert the values
        //bloodGroupTV.setText("klsdjfkdslfj");
        //districTV.setText(district);
        //ageTV.setText(age);

        //getLoggedUserInfor();

        Toast.makeText(this, "Successfully Update!!", Toast.LENGTH_SHORT).show();
        dialog.dismiss();

    }
}
