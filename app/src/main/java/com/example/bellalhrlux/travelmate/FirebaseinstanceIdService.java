package com.example.bellalhrlux.travelmate;

import android.util.Log;

import com.example.bellalhrlux.travelmate.activities.MainActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseinstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("Firebase", refreshedToken);
        //MainActivity.setFirebaseToken(refreshedToken);
    }
}
