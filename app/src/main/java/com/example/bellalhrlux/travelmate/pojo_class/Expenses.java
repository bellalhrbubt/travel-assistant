package com.example.bellalhrlux.travelmate.pojo_class;

public class Expenses {
    private String expenseId;
    private String amount;
    private String description;
    private String createdTime;

    public Expenses(String expenseId, String amount, String description, String createdTime) {
        this.expenseId = expenseId;
        this.amount = amount;
        this.description = description;
        this.createdTime = createdTime;
    }

    public Expenses(String amount, String description, String createdTime) {
        this.amount = amount;
        this.description = description;
        this.createdTime = createdTime;
    }

    public Expenses()
    {

    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
