package com.example.bellalhrlux.travelmate;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.pojo_class.Users;

import java.util.ArrayList;

public class Blood_donor_details extends AppCompatActivity {
    ArrayList<Users> getDonorInfor=new ArrayList<>();
    private TextView donorNameTV,donorEmailTV,donorAgeTV,donorContactTV,donorBloodGpTV,donorDistrictTV;
    private ImageView donorCallDialBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_blood_donor_details);
        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Initialize of layout attribuits
        donorNameTV=findViewById(R.id.donorName);
        donorEmailTV=findViewById(R.id.donorEmail);
        donorAgeTV=findViewById(R.id.donorAge);
        donorContactTV=findViewById(R.id.donorContactNo);
        donorBloodGpTV=findViewById(R.id.donorBloodGroup);
        donorDistrictTV=findViewById(R.id.donorDistrict);
        donorCallDialBtn=findViewById(R.id.donorCallIcon);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("bandle");
        getDonorInfor= (ArrayList<Users>) args.getSerializable("donorList");
        final int index=Integer.valueOf(intent.getExtras().get("index").toString());
        //Toast.makeText(this, ""+getDonorInfor.get(index).getUserName(), Toast.LENGTH_SHORT).show();

        donorNameTV.setText(getDonorInfor.get(index).getUserName());
        donorEmailTV.setText(getDonorInfor.get(index).getUserEmail());
        donorContactTV.setText(getDonorInfor.get(index).getUserPhone());
        if(getDonorInfor.get(index).getBlood_group().equals(""))
        {
            donorBloodGpTV.setText("N/A");
        }
        else
        {
            donorBloodGpTV.setText(getDonorInfor.get(index).getBlood_group());
        }
        if(getDonorInfor.get(index).getDistric().equals(""))
        {
            donorDistrictTV.setText("N/A");
        }
        else
        {
            donorDistrictTV.setText(getDonorInfor.get(index).getDistric());
        }
        if(getDonorInfor.get(index).getAge().equals(""))
        {
            donorAgeTV.setText("N/A");
        }
        else
        {
            donorAgeTV.setText(getDonorInfor.get(index).getAge());
        }



        //change drawable icon color
        int color = Color.parseColor("#1CA261");
        donorCallDialBtn.setColorFilter(color);

        donorCallDialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value=getDonorInfor.get(index).getUserPhone();
                Intent intent=new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+value));
                startActivity(intent);
            }
        });



    }

    //set true on activity back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
