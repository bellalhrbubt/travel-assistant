package com.example.bellalhrlux.travelmate.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.adapter_class.FireServiceAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Hospital_fm extends Fragment {

    String[] fireServiceNameList;
    String[] fireServiceContactsList;
    private ListView fireServiceContactLV;
    public Hospital_fm() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_hospital_fm, container, false);
        ListView listView=view.findViewById(R.id.hospitalServieLV);
        fireServiceNameList=getResources().getStringArray(R.array.hospitalName);
        fireServiceContactsList=getResources().getStringArray(R.array.hospitalContacts);
        FireServiceAdapter fireServiceAdapter=new FireServiceAdapter(getActivity(),fireServiceNameList,fireServiceContactsList);
        listView.setAdapter(fireServiceAdapter);
        return view;
    }

}
