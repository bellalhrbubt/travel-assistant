package com.example.bellalhrlux.travelmate.custom_utils;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.activities.HomeEventActivity;
import com.google.firebase.database.DatabaseReference;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemUtils {
    static boolean bool;
    public static String currentDate()
    {
        Date d=new Date();
        SimpleDateFormat format=new SimpleDateFormat("dd/M/yyyy");
        String date=format.format(d);
        return date;

    }
    public static String currentDateAndTime()
    {
        Date d=new Date();
        SimpleDateFormat format=new SimpleDateFormat("dd/M/yyyy hh:mm:ss a");
        String date=format.format(d);
        return date;

    }

    public static void dateDifference()
    {
        try {
            //Dates to compare
            String CurrentDate=  "03/24/2015";
            String FinalDate=  "09/26/2017";

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            Log.e("HERE","HERE: " + dayDifference);

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
    }


    public static String printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

//        System.out.printf(
//                "%d days, %d hours, %d minutes, %d seconds%n",
//                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
        //Log.e("Time",elapsedDays+" "+" "+elapsedHours+" "+elapsedMinutes+" "+elapsedSeconds);

        return String.valueOf(elapsedDays);
    }

    public static boolean customAlertDialog(final Context context, final DatabaseReference dbRef, String title, String message)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dbRef.removeValue();
                Toast.makeText(context, "Successfully delete this event!!", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
        return bool;
    }
    public static void messageAlert(final Context context, String title, String message)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }

    public static void customAlertMessage(final Context context, String mesg)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View deleteDialogView = inflater.inflate(R.layout.alert_dialog, null);
        final AlertDialog dialog = new android.support.v7.app.AlertDialog.Builder(context).create();
        TextView setMsg=deleteDialogView.findViewById(R.id.alertMessage);
        Button exitMsg=deleteDialogView.findViewById(R.id.alertExitBtn);
        Button cancelBtn=deleteDialogView.findViewById(R.id.cancelAletBtn);

        setMsg.setText(mesg);

        dialog.setView(deleteDialogView);


        exitMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity)context).finish();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
