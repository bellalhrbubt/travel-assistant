package com.example.bellalhrlux.travelmate.weather_activities;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bellalhrlux.travelmate.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {
    private List<Forecast.List> forecastList;
    public Context context;
    private String unitType="C";


    public ForecastAdapter(List<Forecast.List> list)
    {
        this.forecastList=list;
    }

    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View v=layoutInflater.inflate(R.layout.forcast_row,parent,false);
        ForecastViewHolder holder=new ForecastViewHolder(v);
        //context=parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        String convertDate=userDateFormat(forecastList.get(position).getDt()*1000L);
        holder.date.setText(convertDate);
        double temp;
        if(SystemUtils.isCelsius)
        {
            temp=forecastList.get(position).getMain().getTemp();

        }
        else {
            double getTemp=forecastList.get(position).getMain().getTemp();
            temp=SystemUtils.celsiusToFahrenhit(getTemp);
            unitType="F";
        }
        //Toast.makeText(context, "System bool: "+SystemUtils.isCelsius, Toast.LENGTH_SHORT).show();
        //Log.e("Toast",""+SystemUtils.isCelsius);
        holder.temp.setText(""+temp+(char)0x00B0+unitType);
        holder.rain.setText("Weather : "+forecastList.get(position).getWeather().get(0).getMain()+"");
        holder.humidity.setText("Humidity : "+forecastList.get(position).getMain().getHumidity()+" %");
        holder.pressure.setText("Pressure : "+forecastList.get(position).getMain().getPressure()+" hPa");
        holder.winds.setText("Wind : "+forecastList.get(position).getWind().getSpeed()+" m/s");
        //forecastList.get(position).getDt();
        //int getDate=forecastList.get(position).getDt();
        //Log.e("date",userDateFormat((long)(getDate)));

        String photo = "https://openweathermap.org/img/w/"+forecastList.get(position).getWeather().get(0).getIcon()+".png";
        Picasso.get().load(Uri.parse(photo)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return forecastList.size();
    }

    public class ForecastViewHolder extends RecyclerView.ViewHolder{
        TextView temp,winds,pressure,humidity,rain,date;
        ImageView imageView;
        public ForecastViewHolder(View itemView) {
            super(itemView);

            temp=itemView.findViewById(R.id.foreTempTV);
            winds=itemView.findViewById(R.id.foreWindTV);
            pressure=itemView.findViewById(R.id.forePressureTV);
            humidity=itemView.findViewById(R.id.foreHumidityTV);
            imageView=itemView.findViewById(R.id.imageView);
            rain=itemView.findViewById(R.id.foreRain);
            date=itemView.findViewById(R.id.date);
        }

    }
    public String userDateFormat(long timeStamp)
    {
        Date d=new Date(timeStamp);
        SimpleDateFormat dateFormat=new SimpleDateFormat("EEEE \n dd-MM-yyyy hh:mm a");
        String val=dateFormat.format(d);
        return val;
    }
}
