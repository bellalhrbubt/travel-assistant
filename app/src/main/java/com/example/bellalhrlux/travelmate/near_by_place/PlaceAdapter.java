package com.example.bellalhrlux.travelmate.near_by_place;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;

import java.util.List;

public class PlaceAdapter extends ArrayAdapter<PlaceDetailsPojo.Result> {
    private List<PlaceDetailsPojo.Result> placeList;
    Context context;
    public PlaceAdapter(@NonNull Context context, List<PlaceDetailsPojo.Result> listItem) {
        super(context, R.layout.nearest_place_row,listItem);
        this.context=context;
        placeList =listItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

           LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           convertView=layoutInflater.inflate(R.layout.nearest_place_row,parent,false);
           TextView placeName=convertView.findViewById(R.id.placeName);
           TextView placeAddress=convertView.findViewById(R.id.placeAddress);


           if(placeList.size()>0)
           {
               placeName.setText(placeList.get(position).getName()+"");
               String result = Html.fromHtml(placeList.get(position).getAdrAddress()).toString();
               placeAddress.setText(result+"");
               //placeList.get(position).getGeometry().getLocation().getLat();
           }


           return convertView;

    }
}
