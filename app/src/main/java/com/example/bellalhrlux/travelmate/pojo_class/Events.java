package com.example.bellalhrlux.travelmate.pojo_class;

import java.io.Serializable;

public class Events implements Serializable {
    private String eventId;
    private String eventName;
    private String eventStartLoc;
    private String eventDestination;
    private String eventDepatureDate;
    private String eventEstimateBudget;
    private String eventCreatedDate;

    public Events(String eventId, String eventName, String eventStartLoc, String eventDestination,
                  String eventDepatureDate, String eventEstimateBudget, String eventCreatedDate) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventStartLoc = eventStartLoc;
        this.eventDestination = eventDestination;
        this.eventDepatureDate = eventDepatureDate;
        this.eventEstimateBudget = eventEstimateBudget;
        this.eventCreatedDate = eventCreatedDate;
    }

    public Events(String eventName, String eventStartLoc, String eventDestination, String eventDepatureDate, String eventEstimateBudget, String eventCreatedDate) {
        this.eventName = eventName;
        this.eventStartLoc = eventStartLoc;
        this.eventDestination = eventDestination;
        this.eventDepatureDate = eventDepatureDate;
        this.eventEstimateBudget = eventEstimateBudget;
        this.eventCreatedDate = eventCreatedDate;
    }
    public Events()
    {

    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartLoc() {
        return eventStartLoc;
    }

    public void setEventStartLoc(String eventStartLoc) {
        this.eventStartLoc = eventStartLoc;
    }

    public String getEventDestination() {
        return eventDestination;
    }

    public void setEventDestination(String eventDestination) {
        this.eventDestination = eventDestination;
    }

    public String getEventDepatureDate() {
        return eventDepatureDate;
    }

    public void setEventDepatureDate(String eventDepatureDate) {
        this.eventDepatureDate = eventDepatureDate;
    }

    public String getEventEstimateBudget() {
        return eventEstimateBudget;
    }

    public void setEventEstimateBudget(String eventEstimateBudget) {
        this.eventEstimateBudget = eventEstimateBudget;
    }

    public String getEventCreatedDate() {
        return eventCreatedDate;
    }

    public void setEventCreatedDate(String eventCreatedDate) {
        this.eventCreatedDate = eventCreatedDate;
    }
}
