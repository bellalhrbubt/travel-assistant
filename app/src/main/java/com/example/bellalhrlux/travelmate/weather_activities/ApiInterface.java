package com.example.bellalhrlux.travelmate.weather_activities;

import com.example.bellalhrlux.travelmate.near_by_place.NearByPlacePojo;
import com.example.bellalhrlux.travelmate.near_by_place.PlaceDetailsPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiInterface {
    @GET()
    Call<CurrentWeather> getCurrentWeather(@Url String urlString);
    @GET()
    Call<WeatherLocation> getWeatherLocation(@Url String urlString);
    @GET()
    Call<Forecast> getForecast(@Url String urlString);
    @GET()
    Call<NearByPlacePojo> getNearPlace(@Url String urlString);
    @GET()
    Call<PlaceDetailsPojo> getPlaceDetails(@Url String urlString);
}
