package com.example.bellalhrlux.travelmate.adapter_class;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bellalhrlux.travelmate.R;
import com.example.bellalhrlux.travelmate.pojo_class.Users;

import java.util.ArrayList;
import java.util.List;

public class DonorListAdpater extends ArrayAdapter<Users> {
    private ArrayList<Users> donorList;
    Context context;
    public DonorListAdpater(@NonNull Context context, ArrayList<Users> listItem) {
        super(context, R.layout.row_donor_list,listItem);
        this.context=context;
        this.donorList =listItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=layoutInflater.inflate(R.layout.row_donor_list,parent,false);
        TextView donor_name_TV=convertView.findViewById(R.id.donor_name);
        TextView donor_contact_TV=convertView.findViewById(R.id.donor_contact);
        TextView bloodGroup=convertView.findViewById(R.id.blood_group);
        TextView district=convertView.findViewById(R.id.distric);

        donor_name_TV.setText(donorList.get(position).getUserName());
        donor_contact_TV.setText(donorList.get(position).getUserPhone());
        if(donorList.get(position).getBlood_group().equals(""))
        {
            bloodGroup.setText("N/A");
        }
        else {
            bloodGroup.setText(donorList.get(position).getBlood_group());
        }
        if(donorList.get(position).getDistric().equals(""))
        {
            district.setText("N/A");
        }
        else {
            district.setText(donorList.get(position).getDistric());
        }



        return convertView;
    }
}
