package com.example.bellalhrlux.travelmate;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.bellalhrlux.travelmate.fragments.Fire_service_fm;
import com.example.bellalhrlux.travelmate.fragments.Hospital_fm;
import com.example.bellalhrlux.travelmate.fragments.Police_fm;

public class EmergencyContacts extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int numberOfTab;
    private TestPagerAdapter testPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.emergency_contacts);
        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initialize of tab layout
        tabLayout=findViewById(R.id.tabLayoutId);
        viewPager=findViewById(R.id.viewPagerId);

        tabLayout.addTab(tabLayout.newTab().setText("Fire Services"));
        tabLayout.addTab(tabLayout.newTab().setText("Hospital"));
        tabLayout.addTab(tabLayout.newTab().setText("Bangladesh Police"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f69465"));
        tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#000000"));

        numberOfTab=tabLayout.getTabCount();

        testPagerAdapter=new TestPagerAdapter(getSupportFragmentManager(),numberOfTab);
        viewPager.setAdapter(testPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }

    //set true on activity back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private class TestPagerAdapter extends FragmentPagerAdapter {
        int totalTab;
        public TestPagerAdapter(FragmentManager fm, int tabCount) {
            super(fm);
            this.totalTab=tabCount;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position)
            {
                case 0:
                    return new Fire_service_fm();
                case 1:
                    return new Hospital_fm();
                case 2:
                    return new Police_fm();
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return totalTab;
        }
    }
}
