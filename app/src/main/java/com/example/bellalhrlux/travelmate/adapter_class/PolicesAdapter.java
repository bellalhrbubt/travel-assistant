package com.example.bellalhrlux.travelmate.adapter_class;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bellalhrlux.travelmate.R;


public class PolicesAdapter extends BaseAdapter {
    String[] fireCenterNameList;
    String[] fireContactsList;
    Context context;
    ImageView userDial;
    String[] separated;
    String value="";
    String contact_name="";
    private final String service_name="Fire Service";
    public PolicesAdapter(Context context, String[] fireCenterNameList, String[] fireContactsList)
    {
        this.context=context;
        this.fireCenterNameList = fireCenterNameList;
        this.fireContactsList = fireContactsList;
    }

    @Override
    public int getCount() {
        return fireCenterNameList.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view=LayoutInflater.from(context).inflate(R.layout.row_fire_service,viewGroup,false);
        TextView contactName=view.findViewById(R.id.fire_service_name);
        TextView contacts=view.findViewById(R.id.fire_service_contacts);
        final Button buttonOption=view.findViewById(R.id.buttonOptions);
        //userDial=view.findViewById(R.id.user_dial);
        contactName.setText(fireCenterNameList[i]);
        contacts.setText(fireContactsList[i]);
        buttonOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*PopupMenu popupMenu=new PopupMenu(context,buttonOption);
                popupMenu.inflate(R.menu.crud_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId())
                        {
                            case R.id.call:
                                String currentString =fireContactsList[i]; //this line user for separated to the string
                                String[] separated = currentString.split(",");
                                //Toast.makeText(context, ""+separated[0]+" second "+separated[1], Toast.LENGTH_SHORT).show();
                                userCallOptions();
                                *//*Intent intent=new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:"+fireContactsList[i]));
                                context.startActivity(intent);*//*
                                break;
                            case R.id.share:
                                Toast.makeText(context, "Share this contact", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();*/
                contact_name=fireCenterNameList[i];
                userCallOptions(i);

            }
        });
        /*userDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+fireContactsList[i]));
                context.startActivity(intent);
            }
        });*/
        return view;

    }

    private void userCallOptions(final int index)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View deleteDialogView = inflater.inflate(R.layout.alert_contact_list, null);
        final AlertDialog dialog = new AlertDialog.Builder(context).create();
        //initialize of attribuits
        final Spinner spinner=deleteDialogView.findViewById(R.id.contactListSpinner);
        Button callBtn=deleteDialogView.findViewById(R.id.call_dial_btn);
        Button cancelBtn=deleteDialogView.findViewById(R.id.cancelBtn);
        ImageView shareContacts=deleteDialogView.findViewById(R.id.shareContact);
        dialog.setTitle("Select your options");
        dialog.setView(deleteDialogView);

        //add spinner items
        String currentString =fireContactsList[index]; //this line user for separated to the string
        final String[] separated = currentString.split(",");

        ArrayAdapter<String> adapter=new ArrayAdapter<>(context,R.layout.spinner_row,R.id.spinnerValueTV,separated);
        spinner.setAdapter(adapter);


        value=spinner.getSelectedItem().toString();


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                value=separated[i].toString();
                //Toast.makeText(context, ""+value, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
       // Toast.makeText(context, ""+value, Toast.LENGTH_SHORT).show();

        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+value));
                context.startActivity(intent);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        shareContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg=contact_name+"\n"+value;
                String title="Contact Number : "+contact_name+" "+service_name;
                Intent intent=new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,title);
                intent.putExtra(Intent.EXTRA_TEXT,msg);
                context.startActivity(Intent.createChooser(intent,"Share contact"));
            }
        });
        dialog.show();
    }


}
