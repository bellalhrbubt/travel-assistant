package com.example.bellalhrlux.travelmate.adapter_class;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bellalhrlux.travelmate.pojo_class.Events;
import com.example.bellalhrlux.travelmate.custom_utils.SystemUtils;
import com.example.bellalhrlux.travelmate.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EventsAdapter extends ArrayAdapter<Events> {
    private List<Events> eventsList;
    Context context;
    public EventsAdapter(@NonNull Context context, List<Events> listItem) {
        super(context, R.layout.events_row,listItem);
        this.context=context;
        eventsList =listItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=layoutInflater.inflate(R.layout.events_row,parent,false);
        TextView eventNameTV=convertView.findViewById(R.id.eventName);
        TextView eventCreatedTV=convertView.findViewById(R.id.eventCreatedDate);
        TextView eventStartTV=convertView.findViewById(R.id.eventStartDate);
        TextView eventWaitingTV=convertView.findViewById(R.id.waitingTime);
       // Toast.makeText(context, "Value is "+eventsList.get(position).getEventName(), Toast.LENGTH_SHORT).show();
        eventNameTV.setText(eventsList.get(position).getEventName()+"");
        eventCreatedTV.setText("Created on : "+eventsList.get(position).getEventCreatedDate()+"");
        eventStartTV.setText("Start on : "+eventsList.get(position).getEventDepatureDate()+"");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");


        try {
            Date startDate = simpleDateFormat.parse(eventsList.get(position).getEventDepatureDate());
            Date currentDate = simpleDateFormat.parse(SystemUtils.currentDate());
            long longDay=Long.valueOf(SystemUtils.printDifference(currentDate,startDate));
            if(longDay>0)
            {
                eventWaitingTV.setText(SystemUtils.printDifference(currentDate,startDate)+" days left");
            }
            else if(longDay==0)
            {
                eventWaitingTV.setText(" Running event");
            }
            else {
                eventWaitingTV.setText(" Finished");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return convertView;
    }
}
