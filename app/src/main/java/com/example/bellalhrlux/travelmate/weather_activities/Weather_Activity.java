package com.example.bellalhrlux.travelmate.weather_activities;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import retrofit2.Response;

public class Weather_Activity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int tabCount;
    private TestPagerAdapter testPagerAdapter;

    private double latitude = 23.750854;
    private double longitude = 90.393527;
    private String tempUnit = "metric";
    ApiInterface apiInterface;
    private static final String BASE_URL_CURRENT = "http://api.openweathermap.org/data/2.5/";
    Response<WeatherLocation> getResponse;
    private String cityName="";
    private String sendCity="";


    //get location

    private FusedLocationProviderClient client;
    private double latitude_value, longitude_value;
    private LocationRequest request;
    private LocationCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_);

        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //GET SEARCHING RESULTS
        try{
            Intent intent = getIntent();
            if(intent.getAction().equals(Intent.ACTION_SEARCH)){
                String query = intent.getStringExtra(SearchManager.QUERY);
                cityName=query;
                //retrofitConnection();
                //Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e)
        {
            //Toast.makeText(this, "No execute", Toast.LENGTH_SHORT).show();
            Log.e("intent",""+e);
        }

        //INITIALIZE OF LOCATION ATTRIBUTIES
        client = LocationServices.getFusedLocationProviderClient(this);
        request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //Toast.makeText(this, "before interval points ", Toast.LENGTH_SHORT).show();

        request.setInterval(10000);
        request.setFastestInterval(5000);


        tabLayout=findViewById(R.id.tabLayoutId);
        viewPager=findViewById(R.id.viewPagerId);

        tabLayout.addTab(tabLayout.newTab().setText("Current"));
        tabLayout.addTab(tabLayout.newTab().setText("Forecast"));

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f69465"));
        tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#000000"));

        tabCount=tabLayout.getTabCount();
        testPagerAdapter=new TestPagerAdapter(getSupportFragmentManager(),tabCount);
        viewPager.setAdapter(testPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));



    }
    //back button action method
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    private class TestPagerAdapter extends FragmentPagerAdapter {
        private int numberOfTab;
        public TestPagerAdapter(FragmentManager fm, int totalTab) {
            super(fm);
            this.numberOfTab=totalTab;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position)
            {
                case 0:
                    //Toast.makeText(MainActivity.this, "Fragment one", Toast.LENGTH_SHORT).show();
                    WeatherFrag weatherFrag= new WeatherFrag();


                    //retrofit connection method
                    // retrofitConnection();
                    //Toast.makeText(MainActivity.this, "before weather notification", Toast.LENGTH_SHORT).show();
                    weatherFrag.getApiResponse(cityName);

                    return weatherFrag;

                case 1:
                    // Toast.makeText(MainActivity.this, "Fragment two", Toast.LENGTH_SHORT).show();

                    ForecastFrag forecastFrag=new ForecastFrag();
                    forecastFrag.getUserSearchCity(cityName);
                    return  forecastFrag;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return numberOfTab;
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        SearchManager manager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        MenuItem fahrenhit=menu.findItem(R.id.fahrenheit);
        MenuItem celsius=menu.findItem(R.id.celsius);
        if(SystemUtils.isCelsius)
        {
            celsius.setVisible(false);
            fahrenhit.setVisible(true);
        }
        else {
            celsius.setVisible(true);
            fahrenhit.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = getIntent();
        switch (item.getItemId())
        {
            case R.id.fahrenheit:

                finish();
                startActivity(intent);
                SystemUtils.isCelsius=false;
                //Toast.makeText(this, "Farhenhite is selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.celsius:
                finish();
                startActivity(intent);
                SystemUtils.isCelsius=true;
                //Toast.makeText(this, "Celcius is selected", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        SystemUtils.isCelsius=true;
        super.onStop();
    }
}
