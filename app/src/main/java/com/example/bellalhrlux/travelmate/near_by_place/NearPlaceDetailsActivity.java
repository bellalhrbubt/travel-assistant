package com.example.bellalhrlux.travelmate.near_by_place;

import android.graphics.Color;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.bellalhrlux.travelmate.Directions.DirectionResponse;
import com.example.bellalhrlux.travelmate.Directions.DirectionService;
import com.example.bellalhrlux.travelmate.Directions.Step;
import com.example.bellalhrlux.travelmate.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NearPlaceDetailsActivity extends AppCompatActivity implements OnMapReadyCallback{
    private GoogleMap map;
    private GoogleMapOptions mapOptions;
    double latitude,longitude,currentLat,currentLan;

    private DirectionService service;
    private Button insBtn;
    private String[]instructions;
    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_place_details);
        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        retrofitConnetciton();
         latitude=getIntent().getExtras().getDouble("lat");
         longitude=getIntent().getExtras().getDouble("lan");
         currentLat=getIntent().getExtras().getDouble("currentLat");
         currentLan=getIntent().getExtras().getDouble("currentLan");

        mapOptions=new GoogleMapOptions();
        mapOptions.zoomControlsEnabled(true);

        SupportMapFragment mapFragment=SupportMapFragment.newInstance(mapOptions);
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction()
                .replace(R.id.mapLoadContainst,mapFragment);
        ft.commit();
        mapFragment.getMapAsync(this);
    }

    //back button action method
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;

        LatLng latLng=new LatLng(latitude,longitude);
        LatLng currentLatLan=new LatLng(currentLat,currentLan);
        map.addMarker(new MarkerOptions().position(latLng));
        map.addMarker(new MarkerOptions().position(currentLatLan));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLan,17));
        getDirections();
    }

    private void retrofitConnetciton()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(DirectionService.class);
    }

    private void getDirections() {
        String key = getString(R.string.direction_api_key);
        String currentLatLan=currentLat+","+currentLan;
        String selectedLatLan=latitude+","+longitude;
        String endUrl = String.format("directions/json?origin=%s&destination=%s&key=%s",currentLatLan,selectedLatLan,key);
        Call<DirectionResponse> call = service.getDirections(endUrl);
        call.enqueue(new Callback<DirectionResponse>() {
            @Override
            public void onResponse(Call<DirectionResponse> call, Response<DirectionResponse> response) {
                if(response.code() == 200){
                    DirectionResponse directionResponse = response.body();

                    List<Step> steps = directionResponse.getRoutes().get(0).getLegs().get(0).getSteps();
                    instructions = new String[steps.size()];
                    //insBtn.setVisibility(View.VISIBLE);
                    for(int i = 0; i < steps.size(); i++){
                        double startLat = steps.get(i).getStartLocation().getLat();
                        double startLon = steps.get(i).getStartLocation().getLng();

                        double endLat = steps.get(i).getEndLocation().getLat();
                        double endLon = steps.get(i).getEndLocation().getLng();

                        LatLng start = new LatLng(startLat,startLon);
                        LatLng end = new LatLng(endLat,endLon);

                        String insString = String.valueOf(Html.fromHtml(steps.get(i).getHtmlInstructions()));
                        instructions[i] = insString;

                        Polyline polyline = map.addPolyline(new PolylineOptions()
                                .add(start)
                                .add(end)
                                .color(0xff388E3C)
                                .endCap(new RoundCap())
                                .jointType(JointType.ROUND)
                                .width(20)
                                .clickable(true));

                        polyline.setTag(insString);
                    }
                }
            }

            @Override
            public void onFailure(Call<DirectionResponse> call, Throwable t) {

            }
        });
    }
}
