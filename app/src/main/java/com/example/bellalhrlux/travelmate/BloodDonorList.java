package com.example.bellalhrlux.travelmate;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bellalhrlux.travelmate.adapter_class.DonorListAdpater;
import com.example.bellalhrlux.travelmate.pojo_class.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class BloodDonorList extends AppCompatActivity {
    private DatabaseReference rootRef,eventRef,userRef, eventListRef,sendDbRef,logedUserRef;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private ArrayList<Users> getUsersList=new ArrayList<>();
    private ListView donorLV;
    private TextView errorMsgTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_blood_donor_list);
        //set display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //initialize of firebase
        auth=FirebaseAuth.getInstance();
        user=auth.getCurrentUser();
        rootRef= FirebaseDatabase.getInstance().getReference();
        userRef=rootRef.child("Profile");
        //Initialize of layout attributies
        donorLV=findViewById(R.id.donor_listLV);
        errorMsgTV=findViewById(R.id.errorMsgTV);
        //get total user list
        getAllUsers();

        donorLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(BloodDonorList.this,Blood_donor_details.class);
                Bundle args = new Bundle();
                args.putSerializable("donorList",(Serializable)getUsersList);
                intent.putExtra("bandle",args);
                intent.putExtra("index",i);
                startActivity(intent);

                //Toast.makeText(BloodDonorList.this, ""+getUsersList.get(i).getUserName(), Toast.LENGTH_SHORT).show();
            }
        });

        //searching code start
        try{
            Intent intent = getIntent();
            if(intent.getAction().equals(Intent.ACTION_SEARCH)){
                final String query = intent.getStringExtra(SearchManager.QUERY);

                //retrofitConnection();
               // Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        getUsersList.clear();
                        for(DataSnapshot snapshot:dataSnapshot.getChildren())
                        {
                            Users users=snapshot.getValue(Users.class);//this not efficient way
                            if(users.getBlood_group().equals(query))
                            {
                                getUsersList.add(users);
                            }
                        }
                        if(getUsersList.size()>0)
                        {
                            DonorListAdpater donorListAdpater=new DonorListAdpater(BloodDonorList.this,getUsersList);
                            donorLV.setAdapter(donorListAdpater);
                        }
                        else {
                            errorMsgTV.setText("Does not found this blood group!");
                           //Toast.makeText(BloodDonorList.this, "Does not found this blood group!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
        catch (Exception e)
        {
            //Toast.makeText(this, "No execute", Toast.LENGTH_SHORT).show();
            Log.e("intent",""+e);
        }
        //searching code end


    }

    private void getAllUsers() {
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getUsersList.clear();
                    for(DataSnapshot snapshot:dataSnapshot.getChildren())
                    {
                        Users users=snapshot.getValue(Users.class);
                        if(!users.getUserId().equals(user.getUid()))
                        {
                            getUsersList.add(users);
                        }

                    }

                DonorListAdpater donorListAdpater=new DonorListAdpater(BloodDonorList.this,getUsersList);
                donorLV.setAdapter(donorListAdpater);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //set true on activity back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        SearchManager manager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        return true;
    }
}
